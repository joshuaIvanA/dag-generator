from datetime import datetime, timedelta
from airflow import DAG
from aiflow.models import Variable
from airflow.contrib.operators.bigquery_operator import BigQueryOperator
from operators.start_operator import StartOperator
from operators.finish_operator import FinishOperator
import os

script_location = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'scripts') 

with DAG(dag_id='', schedule_interval=''default_args=) as dag:
	
	task_start_dag = StartOperator(
		task_id='start_dag_{}'.format(dag.dag_id)
	)

	task_ = BigQueryOperator(
		task_id='task_', 
		sql=os.path.join(script_location, ''),
		destination_dataset_table=,
		allow_large_result=True,
		bigquery_conn_id=,
		use_legacy_sql=False,
		write_disposition=''
		create_disposition=''
	)

	task_finish_dag = FinishOperator(
		task_id='finish_dag_{}'.format(dag.dag_id)
	)

task_start_dag >> task_ >> task_finish_dag
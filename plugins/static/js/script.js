const defaultDag = {
	dag_id : '',
	scheduler : '00:00:00',
	day_week: '0',
	day_month: '1',
	interval: 'daily',
	email: '',
	write_disposition: 'WRITE_EMPTY',
	create_disposition: 'CREATE_IF_NEEDED',
	priority: 'INTERACTIVE',
	use_legacy_sql: false,
	allow_large_results: true,
	tasks: []
}
const defaultTask = {
	task_id: '',
	sql: '',
	parents: [],
	upload: false,
	showDropdown: false
}
var app = new Vue({
	el: '#app',
	data :{	
		dag: {...defaultDag},
		notification: {
			showNotif: false,
			message: '',
			error: false,
			success: true
		},
		active_dag: '',
		files : [],
		dag_list: [],
		form_status: 'create',
		errors: {}
	},
	delimiters: ['[[',']]'],
	created() {
		this.addTask()
		this.retrieveAllDAG()
	},
	methods: {
		apiGetCall(data, onSuccess, onError){
			let csrf_token = document.getElementsByName('csrftoken')[0].value
			fetch(data.url, 
			{
				method: data.method,
				headers: {"X-CSRFToken": csrf_token }
			})
			.then((response)=>{
				if(response.status!==200){ onError(false, 'Internal Server Error') }
				return response.json();
			})
			.then((response)=>{
				if(response.success) {onSuccess(response)}	
				else onError(response)		
			})
			.catch((error)=>{
				onError(error)
			})

		},
		apiUpsertCall(data, onSuccess, onError){
			let csrf_token = document.getElementsByName('csrftoken')[0].value
			fetch(data.url, 
			{
				method: data.method,
				headers: {"X-CSRFToken": csrf_token },
				body: data.formData
			})
			.then((response)=>{
				if(response.status==200){ onError(false, 'Internal Server Error') };
				return response.json();
			})
			.then((response)=>{
				if(response.success) {onSuccess(response)}	
				else onError(response)		
			})
			.catch((error)=>{
				onError(error)
			})

		},
		notifyCall: function(status, message) {
			this.notification = {...this.notification, showNotif: true, success: status, error: !status, message: message}
			setTimeout(()=>{
				this.notification = {...this.notification, showNotif: false}
			}, 2000)				
		},
		displayDiv: function(key, param){
			let switchcase = null;
			switch(key){
				case 'upload':
					switchcase = this.dag.tasks[param.index].upload; 
					if(switchcase)document.getElementsByName('sql-file')[param.index].value = ''
					return param.reversed?(switchcase?'hidden':'show'):(switchcase?'show':'hidden');
				case 'notify':
					switchcase = this.notification.showNotif;
					return {active: switchcase, 'alert-danger': this.notification.error, 'alert-success': this.notification.success};
				case 'dropdown': 
					return {active: this.dag.tasks[param.index].showDropdown}
				case 'activeDag':
					return {active: (param.id === this.active_dag)}
				case 'checkError':
					return {error: (this.errors[param.key]? true: false)}
				}
			return null;				
		},
		checkParents: function(root, value){
			let pTask = null
			let list = []
			value.parents.forEach((pValue)=>{
				if(pValue===root.task_id){
					list.push(false)
				}else if(root.parents.includes(pValue)){
					list.push(false)
				}else { 
					this.dag.tasks.forEach((task)=>{
						if(task.task_id===pValue)pTask=task
					})
					if(pTask.parents.includes(root.task_id)) list.push(false)
					else if(pTask.parents.length===0) list.push(true)
					else list.push(this.checkParents(root, pTask))
				}
			})
			return list
		},
		checkCycle: function(root, value){
			let result = []
			if(root.parents.includes(value)) return true
			else if((root.task_id == value.task_id) || value.task_id==='') return false
			else if(value.parents.length>0){result = this.checkParents(root, value)}

			if(result.includes(false)) return false
			else return true
		},
		handleFileUpload(index){
			this.files[index] = this.$refs.file[0].files[0]
		},
		addTask: function(task={...defaultTask, parents: []}){
			this.dag.tasks.push(task)
			this.files.push(new File([""], "temp.txt"))
		},
		deleteTask: function(index){
			let task_id = this.dag.tasks[index].task_id
			this.dag.tasks.splice(index, 1)
			this.files.splice(index, 1)

			this.dag.tasks.forEach((task)=>{
				if(task.parents.includes(task_id)){
					task.parents.splice(task.parents.indexOf(task_id), 1)
				}
			})
		},
		toggleUpload: function(index){
			this.dag.tasks[index].upload = !this.dag.tasks[index].upload;
			if(!this.dag.tasks[index].upload){this.files[index] = {}}
		},
		toggleDropdown: function(index){
			this.dag.tasks[index].showDropdown = !this.dag.tasks[index].showDropdown;
		},
		validateForm: function(){
			this.errors={}
			let task_ids = []
			Object.keys(this.dag).forEach((key)=>{
				switch(key){
					case 'dag_id': if(!(/^[a-zA-Z0-9_]+$/.test(this.dag[key])))this.errors[key]=true
						return
					case 'tasks': this.dag['tasks'].forEach((task, index)=>{
							if(!(/^[a-zA-Z0-9_]+$/.test(task.task_id)))this.errors[`${key}-${index}`]=true
							if(task.upload){
								if(!document.getElementsByName('sql-file')[index].value.includes(this.files[index].name))this.errors[`${key}-${index}`]=true
							}
							else if(task.sql==='') this.errors[`${key}-${index}`]=true
							task_ids.forEach((id)=>{
								if(task.task_id==id)this.errors[`${key}-${index}`]=true
							})
							task_ids.push(task.task_id)
						})	
						return	
				}	
			})
			return Object.keys(this.errors).length
		},
		defaultFormContent: function(){
			this.dag = {...defaultDag, tasks: []}
			this.form_status = 'create'
			this.files = []
			this.active_dag = ''
			this.addTask()
		},
		onSubmit: function(){
			if(this.form_status == 'update') this.editDAG()
			else if(this.validateForm()===0) this.createDAG()
		},
		retrieveAllDAG: function(){
			this.apiGetCall({url: '/admin/plugin/dag', method: 'get'}, 
			(response)=>{
				this.dag_list = response.data
				this.notifyCall(response.success, 'Successfully retrieved all DAGs')
			},
			(error)=>{
				this.notifyCall(error.success, 'Failed to retrieve all DAGs')
			})	
		},
		viewDAG: function(id) {
			this.apiGetCall({url: `/admin/plugin/dag/${id}`, method: 'get'},
			(response)=>{
				this.notifyCall(response.success, `Successfully retrieved DAG ${id}`)
				this.setForm(response.data, 'update')
			},
			(error)=>{
				this.notifyCall(error.success, `Failed to retrieve DAG ${id}`)
			})
		},
		editDAG: function(){
			let formData = new FormData();
			formData.append('dag', JSON.stringify(this.dag))
			this.files.forEach((file, idx)=>{
				formData.append(`file-${idx}`, file)	
			})
			this.apiUpsertCall({url: '/admin/plugin/generate', method: 'put', formData: formData},
			(response)=>{
				this.notifyCall(response.success, `Successfully updated DAG ${this.dag.dag_id}`)
				this.defaultFormContent()
				this.retrieveAllDAG()
			},
			(error)=>{
				this.notifyCall(error.success, error.message)
			}) 
		},
		createDAG: function(){
			let formData = new FormData();
			formData.append('dag', JSON.stringify(this.dag))
			this.files.forEach((file, idx)=>{
				formData.append(`file-${idx}`, file)	
			})
			this.apiUpsertCall({url: '/admin/plugin/generate', method: 'post', formData: formData},
			(response)=>{
				this.notifyCall(response.success, `Successfully generated DAG ${this.dag.dag_id}`)
				this.defaultFormContent()
				this.retrieveAllDAG()	
			},
			(error)=>{
				this.notifyCall(error.success, error.message)
			})
		},
		deleteDAG: function(id){
			if(confirm('Are you sure you want to delete this Dag?')){
				this.apiGetCall({url: `/admin/plugin/dag/${id}`, method: 'delete'},
				(response)=>{
					this.notifyCall(response.success, `Successfully deleted DAG ${id}`)
					this.retrieveAllDAG()
				},
				(error)=>{
					this.notifyCall(error.success, `Failed to delete DAG ${id}`)	
				})
			}
		},
		setForm: function(data, status){
			this.dag = {
				dag_id : data.dag_id,
				scheduler : data.scheduler,
				interval: data.interval,
				day_month: data.day_month,
				day_week: data.day_week,
				email: data.email,
				create_disposition: data.create_disposition,
				write_disposition: data.write_disposition,
				use_legacy_sql: false,
				allow_large_results: true,
				priority: data.priority,
				tasks: []
			}
			this.active_dag = data.dag_id
			this.form_status = status
			this.files = []
			this.errors = {}

			data.task_list.forEach((value)=>{
				 let task = {
					...defaultTask,
					task_id: value.task_id,
					sql: value.script_name,
					parents: value.parents,
					upload: false
				}
				this.addTask(task)
			})
		}
	}
})

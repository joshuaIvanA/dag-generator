import logging
import os
import shutil
import json

from flask import Blueprint, request, Response, redirect, jsonify
from flask_admin import BaseView, expose
from datetime import datetime, timedelta
from sets import Set
from jinja2 import Environment, FileSystemLoader
from airflow.hooks.postgres_hook import PostgresHook
from airflow.www.app import csrf
from airflow.plugins_manager import AirflowPlugin

import psycopg2
import psycopg2.extensions
from psycopg2.extras import RealDictCursor

log = logging.getLogger(__name__)

conn = PostgresHook().get_conn()
cur = conn.cursor()
cur.execute('select exists (select * from information_schema.tables where table_name=%s)',('created_dag',))
if(cur.fetchone()[0] == False):
	print('Initialize Created DAG Table')
	cur.execute('create table created_dag (dag_id varchar(256) primary key, cron varchar(256), email varchar(256), create_disposition varchar(256), write_disposition varchar(256), use_legacy_sql boolean, allow_large_results boolean, priority boolean);')
	cur.execute('create table created_task (task_id varchar(256), dag_id varchar(256) references created_dag(dag_id), script_path varchar(256), parents varchar(256), primary key(task_id));')
	conn.commit()
	print('Table Created Successfully!')
cur.close()
conn.close()

class DagGeneratorView(BaseView):
	args = {
		'dag_id' : '',	
		'create_disposition' : '',
		'write_disposition' : '',
		'allow_large_results': '',
		'use_legacy_sql': '',
		'email' : '',
		'schedule': '',
		'priority': ''
	}
	response = {
		'status': '200',
		'success': True,
		'data': [],
		'message': ''
	}
	form={}
	dag_selected={}
	task_selected=[]


	@expose('/', methods=['GET'])
	def index(self):
		return self.render('dag_plugin/index.html', form=self.form)

	@expose('/dag', methods=['GET'])
	def get_all_dag(self):
		self.get_all_dag_db()
		return jsonify(self.response)
	
	def get_all_dag_db(self):
		conn = PostgresHook().get_conn()
		cur = conn.cursor(cursor_factory=RealDictCursor)
		cur.execute('select * from created_dag;')
		self.response['data'] = cur.fetchall()
		cur.close()
		conn.close()

	@expose('/dag/<id>', methods=['GET'])
	def get_dag(self, id):
		self.get_dag_db(id)
		self.convert_cron_to_schedule(self.dag_selected.get('cron'))
		self.get_sql()
		self.response['data']= self.dag_selected	
		return jsonify(self.response)

	def get_dag_db(self, id):
		conn = PostgresHook().get_conn()
		cur = conn.cursor(cursor_factory=RealDictCursor)
		cur.execute("select * from created_dag where dag_id = %s;", (id,))
		self.dag_selected = cur.fetchone()
		
		cur.execute("select * from created_task where dag_id = %s;", (id,))
		self.dag_selected = {
			'dag_id': self.dag_selected.get('dag_id'),
			'cron': self.dag_selected.get('cron'),
			'email': self.dag_selected.get('email'),
			'create_disposition': self.dag_selected.get('create_disposition'),
			'write_disposition': self.dag_selected.get('write_disposition'),
			'allow_large_results': self.dag_selected.get('allow_large_results'),
			'use_legacy_sql': self.dag_selected.get('use_legacy_sql'),
			'priority': self.dag_selected.get('priority'),
			'task_list': [],
			'scheduler': '',
			'interval': ''
		}
		task_temp = cur.fetchall()
		
		for index,value in enumerate(task_temp):
			task = {
			'task_id': value.get('task_id'),
			'script_name': value.get('script_name'),
			'parents': json.loads(value['parents'])
			}
			self.dag_selected.get('task_list').append(task)
				
		cur.close()
		conn.close()

	def convert_cron_to_schedule(self, cron):
		value = cron.split(' ')
		self.dag_selected['day_month'] = 1
		self.dag_selected['day_week'] = 0 
		self.dag_selected['scheduler'] = ("%02d:%02d" % (int(value[1]), int(value[0])))
		self.dag_selected['interval'] = 'daily'
	
		if(value[2] != '*'):
			self.dag_selected['day_month'] = int(value[2])
			self.dag_selected['interval'] = 'monthly'
		elif(value[4] != '*'):
			self.dag_selected['day_week'] = int(value[4])
			self.dag_selected['interval'] = 'weekly'

	def get_sql(self):
		for index,value in enumerate(self.dag_selected.get('task_list')):
			with open(value['script_name'], 'r') as f:
				value['script_name'] = f.read()
				f.close()

	@expose('/dag/<id>', methods=['DELETE'])
	def delete_dag(self, id):
		self.get_task_db(id)	
		self.delete_sql(self.task_selected)
		self.delete_dag_db(id)
		self.delete_dag_file(id)

		return jsonify(self.response)

	def delete_sql(self, task):	
		shutil.rmtree(os.path.dirname(task['script_name']))

	def get_task_db(self, id):
		conn = PostgresHook().get_conn()
		cur = conn.cursor(cursor_factory=RealDictCursor)
		cur.execute("select * from created_task where dag_id = %s;", (id,))
		self.task_selected = cur.fetchone()
		cur.close()
		conn.close()

	def delete_dag_db(self, id):
		conn = PostgresHook().get_conn()
		cur = conn.cursor(cursor_factory=RealDictCursor)
		try:
			cur.execute("delete from created_task where dag_id = %s;", (id,))
			cur.execute("delete from created_dag where dag_id = %s;", (id,))
			conn.commit()
		except Exception as e:
			conn.rollback()
			self.response['status']= 500
			self.response['message']= e.message
			self.response['success']= False
		cur.close()
		conn.close()

	def delete_dag_file(self, id):
		dag_path = os.path.join(os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), 'dags'), id + '.py')
		if(os.path.isfile(dag_path)):
			os.remove(dag_path)
		dag_path = os.path.join(os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), 'dags'), id + '.pyc')
		if(os.path.isfile(dag_path)):
			os.remove(dag_path)
	
	@expose('/generate', methods=['PUT'])
	def update_dag(self):
		self.define_form()
		self.delete_dag(self.form.get('dag_id'))
		return self.generate_dag()

	@expose('/generate', methods=['POST'])
	def generate_dag(self):
		#set dag-values and files
		self.define_form()				

		#set schedule
		self.set_schedule()

		if(self.validate_input()):
			self.define_args()
			for index, task in enumerate(self.form['tasks']):
				#save sql-file
				self.save_sql(task, index)	
			
			#insert to database
			self.insert_to_db(task)

			#save dag-file
			self.save_dag()

			return jsonify(self.response)
		
		self.response['success']= False
		self.response['status']= 400
		self.response['message']= 'Bad Request'
		return jsonify(self.response)
		
	def define_form(self):
		dag = json.loads(request.form.get('dag'))
		for k, v in dag.items():
			self.form[k] = v

		self.form['files'] = []
		for n in range(0, len(self.form['tasks'])):
			self.form['files'].extend(request.files['file-'+ str(n)])

	def validate_input(self):
		for k, v in self.form.items():
			if v == '' and k not in ['tasks', 'use_legacy_sql', 'allow_large_results']:
				return False
			elif k == 'tasks':
				if(len(v) == 0):
					return False
		return True 

	def define_args(self):
		for k, v in self.args.items():
			if (k in self.form.keys()):
				self.args[k] = self.form[k]

	def save_sql(self, task, index):
		script_path = os.path.join(os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), 'dags/scripts'), str(self.form.get('dag_id')))

		task['sql_fname'] = os.path.join(script_path, task['task_id'] + '.sql')	
		task['content'] = self.form['files'][index] if task['upload'] == True else task['sql']
	
		if not os.path.exists(script_path):
			os.makedirs(script_path)

		with open(task['sql_fname'], 'w+') as f:
			f.write(task['content'])
			f.close()

	def save_dag(self):
		fname = os.path.join(os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), 'dags'),str(self.form['dag_id']) + '.py')
		if not os.path.exists(fname):
			with open(fname, 'w+') as f:
				template_file = self.load_template()
				f.write(template_file)
				f.close()
		else:
			self.success = False
		
	def load_template(self):
		env = Environment(loader=FileSystemLoader(['../templates', os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'templates')]))
		template_file = 'dag_template.py.j2'
		template = env.get_template(template_file)
		parents = Set([i for task in self.form['tasks'] for i in task['parents']])
		return template.render(args = self.args, tasks = self.form['tasks'], parents = parents)

	def set_schedule(self):
		scheduler = [int(i) for i in self.form['scheduler'].split(':')]

		self.form['schedule'] =  str(scheduler[1]) +' '+ str(scheduler[0]) + ' * * *'
		if(self.form.get('interval') == 'weekly'):
			self.form['schedule'] = ('%s %s * * %s' % (str(scheduler[1]), str(scheduler[0]),  str(self.form['day_week'])))
		elif(self.form.get('interval') == 'monthly'):
			self.form['schedule'] = ('%s %s %s * *' % (str(scheduler[1]), str(scheduler[0]),  str(self.form['day_month'])))

	def insert_to_db(self, task):
		conn = PostgresHook().get_conn()
		cur = conn.cursor()
		try:
			cur.execute('insert into created_dag values (%s, %s, %s, %s, %s, %s, %s, %s)', (
				self.args.get('dag_id'), 
				self.args.get('schedule'), 
				self.args.get('email'), 
				self.args.get('create_disposition'), 
				self.args.get('write_disposition'), 
				self.args.get('use_legacy_sql'), 
				self.args.get('allow_large_results'),
				self.args.get('priority')))
			for index, task in enumerate(self.form.get('tasks')):
				cur.execute('insert into created_task values (%s, %s, %s, %s)', (
				task['task_id'], 
				self.args.get('dag_id'),
				task['sql_fname'],
				json.dumps(task['parents'])))
			log.info('Successfully saved all task for dag %s', self.args.get('dag_id'))

			conn.commit()
			log.info('Successfully created dag %s', self.args.get('dag_id'))
		except Exception as e:
			log.error(e)
			conn.rollback()
			self.response['status']= 500
			self.response['message']= e.message
			self.response['success']= False
		cur.close()
		conn.close()
	
	def update_to_db(self, task):
		conn = PostgresHook().get_conn()
		cur = conn.cursor()
		try:
			cur.execute('update created_dag set cron = %s, create_disposition = %s, write_disposition = %s, use_legacy_sql = %s, allow_large_results = %s, priority = %s', (
				self.args.get('schedule'),
				self.args.get('create_disposition'),
				self.args.get('write_disposition'),
				self.args.get('use_legacy_sql'),
				self.args.get('allow_large_results'),
				self.args.get('priority')))
			for index, task in enumerate(self.form.get('tasks')):
				cur.execute('insert into created_task values (%s, %s, %s, %s)', (
				task['task_id'], 
				self.args.get('dag_id'),
				task['sql_fname'],
				json.dumps(task['parents'])))
			log.info('Successfully saved all task for dag %s', self.args.get('dag_id'))

			conn.commit()
			log.info('Successfully created dag %s', self.args.get('dag_id'))
		except Exception as e:
			log.error(e)
			conn.rollback()
			self.response['status']= 500
			self.response['message']= e.message
			self.response['success'] = False
		cur.close()
		conn.close()

view = DagGeneratorView(name='BigQuery Scheduler', endpoint='plugin')

bp = Blueprint(
	'dag_tools',__name__,
	template_folder='../templates',
	static_folder='../static',
	static_url_path='/static/plugin')

class DagGeneratorPlugin(AirflowPlugin):
	name='dag_generator_plugin'
	operators=[]
	flask_blueprints=[bp]
	hooks=[]
	executors=[]
	admin_views=[view]
	menu_links=[]
	

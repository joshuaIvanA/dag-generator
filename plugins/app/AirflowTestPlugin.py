import logging
import airflow
import os
import socket
from airflow import configuration
from airflow.plugins_manager import AirflowPlugin
from flask import Blueprint
from flask_admin import BaseView, expose
from flask_admin.base import MenuLink
from jinja2 import Environment, FileSystemLoader
from airflow.hooks.base_hook import BaseHook
from airflow.models import BaseOperator
from airflow.operators.sensors import BaseSensorOperator
from airflow.executors.base_executor import BaseExecutor

log = logging.getLogger(__name__)

class PluginHook(BaseHook):
	pass

class PluginOperator(BaseOperator):
	pass

class PluginSensorOperator(BaseSensorOperator):
	pass

class PluginExecutor(BaseExecutor):
	pass

def plugin_macro():
	pass

class TestView(BaseView):
	@expose('/', methods=['GET'])
	def index(self):
		airflow_version = airflow.__version__
		host_name = socket.gethostname()
		dags_folder = configuration.get('core', 'DAGS_FOLDER')
		dags_folder_contents=[]
		
		for root,subdirs,files in os.walk(dags_folder):
			dags_folder_contents.append(root)
			for file in files:
				dags_folder_contents.append('\t'+file)

		env=Environment(loader=FileSystemLoader('templates'))
		log.debug(env)
		return self.render("test_plugin/index.html",
					airflow_version=airflow_version,
					host_name=host_name,
					content="Hello Galaxy!",
					dags_folder=dags_folder,
					dags_folder_contents=dags_folder_contents)

	@expose('/test', methods=['GET'])
	def test(self):
		log.info('Hello World!')
		return 'Hello'
v = TestView(name='Test View', category='Test Plugin', endpoint='testplugin')

ml = MenuLink(
	category='Test Plugin', 
	name='Test Menu Link', 
	url='https://airflow.incubator.apache.org/')

bp = Blueprint(
	"test_plugin",__name__,
	template_folder='../templates',
	static_folder='../static',
	static_url_path='/static/')

class AirflowTestPlugin(AirflowPlugin):
	name='test_plugin' 
	operators=[PluginOperator] 
	sensors=[PluginSensorOperator] 
	hooks=[PluginHook] 
	executors=[PluginExecutor] 
	macros=[plugin_macro] 
	admin_views=[v] 
	flask_blueprints=[bp] 
	menu_links=[ml] 
